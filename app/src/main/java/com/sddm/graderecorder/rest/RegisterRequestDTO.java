package com.sddm.graderecorder.rest;

import java.io.Serializable;

public class RegisterRequestDTO implements Serializable {

    private String email;
    private String name;
    private String registation_id;

    public RegisterRequestDTO(String email, String name, String registation_id) {
        this.email = email;
        this.name = name;
        this.registation_id = registation_id;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegistation_id() {
        return registation_id;
    }

    public void setRegistation_id(String registation_id) {
        this.registation_id = registation_id;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RegisterRequestDTO{");
        sb.append("email='").append(email).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", registation_id='").append(registation_id).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
