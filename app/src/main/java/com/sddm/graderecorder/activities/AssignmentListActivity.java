package com.sddm.graderecorder.activities;

import java.io.IOException;
import java.util.ArrayList;

import android.accounts.AccountManager;
import android.app.DialogFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.appspot.grade_recorder_sddm.graderecorder.Graderecorder;
import com.appspot.grade_recorder_sddm.graderecorder.model.Assignment;
import com.appspot.grade_recorder_sddm.graderecorder.model.AssignmentCollection;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.json.gson.GsonFactory;
import com.sddm.graderecorder.helpers.QuickstartPreferences;
import com.sddm.graderecorder.R;
import com.sddm.graderecorder.helpers.RegistrationIntentService;
import com.sddm.graderecorder.adapters.AssignmentArrayAdapter;


public class AssignmentListActivity extends AppCompatActivity {

    Toolbar mToolbar;
    ListView mListView;
    LinearLayout mLoadingForm;
    LinearLayout mGcmForm;
    TextView mEmpty;
    TextView mGcmMessage;
    AssignmentArrayAdapter adapter;
    /**
     * Credentials object that maintains tokens to send to the back end.
     */
    GoogleAccountCredential mCredential;

    /**
     * Preference object where we store the name of the current user.
     */
    SharedPreferences mSettings = null;

    // FIXME: Replace this evil global variable with a more robust singleton
    // object.
    static Graderecorder mService;

    public static final String SHARED_PREFERENCES_NAME = "GradeRecorder";
    public static final String PREF_ACCOUNT_NAME = "PREF_ACCOUNT_NAME";
    static final int REQUEST_ACCOUNT_PICKER = 1;
    public static final String GR = "GR";

    static final String KEY_ASSIGNMENT_ENTITY_KEY = "KEY_ASSIGNMENT_ID";
    static final String KEY_ASSIGNMENT_NAME = "KEY_ASSIGNMENT_NAME";
    static final String KEY_SERVICE = "KEY_SERVICE";

    //gcm
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(GR,"Oncreate");
        setContentView(R.layout.activity_assignment_list);
        setToolbar();

        mListView = (ListView) findViewById(R.id.my_listview);
        mLoadingForm = (LinearLayout) findViewById(R.id.empty_form);
        mGcmForm = (LinearLayout) findViewById(R.id.gcm_form);
        mEmpty = (TextView) findViewById(R.id.no_assigments_label);
        mGcmMessage = (TextView) findViewById(R.id.gcm_message);

        //begin gcm


        mGcmForm.setVisibility(ProgressBar.VISIBLE);
        mGcmMessage.setText(getString(R.string.loading_play_services));


        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                mGcmForm.setVisibility(ProgressBar.GONE);
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
                String token = sharedPreferences
                        .getString(QuickstartPreferences.TOKEN_TO_SERVER, null);
                if (sentToken) {
                    Toast.makeText(AssignmentListActivity.this, "Token got:"+token, Toast.LENGTH_SHORT).show();
                    initAssignments();
                } else {
                    Toast.makeText(AssignmentListActivity.this, "Token Error:"+getString(R.string.token_error_message), Toast.LENGTH_LONG).show();

                }
            }
        };

        if (checkPlayServices()) {
            Log.d(GR,"you have PlayServices");
            mGcmMessage.setText(getString(R.string.loading_gcm));
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }

        //end gcm





    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(GR, "OnResume");
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
    }

    @Override
    protected void onPause() {
        Log.d(GR,"OnPause");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    private void initAssignments() {

        mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        mListView.setMultiChoiceModeListener(new MyMultiClickListener());

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                /**
                 * If they click an assignment, we launch an activity to display all that
                 * assignment's grade entries.
                 */

                Intent gradeIntent = new Intent(AssignmentListActivity.this, GradeEntryListActivity.class);
                Assignment assignment = (Assignment) adapter.getItem(position);
                gradeIntent.putExtra(KEY_ASSIGNMENT_NAME, assignment.getName());
                gradeIntent.putExtra(KEY_ASSIGNMENT_ENTITY_KEY, assignment.getEntityKey());
                startActivity(gradeIntent);
            }
        });

        // FIXME: Check to ensure they have Google Play services.

        // TODO: Create a credential object with the client id of the *deployed*
        // web client, NOT the Android client. (Go figure.)
        mCredential = GoogleAccountCredential.usingAudience(this,
                "server:client_id:" + getString(R.string.web_client_id));

        // Easy if only ever 1 user.
        // FIXME: Known issue: since on the backend, we used the user property
        // and not the user ID, this only works if the username is all
        // lowercase. Sorry.
        // mCredential.setSelectedAccountName("username@gmail.com");

        // More robust approach: let the user pick the account, but save it as a
        // shared preference so they don't have to keep logging in every time
        // onCreate is called.
        mSettings = getSharedPreferences(SHARED_PREFERENCES_NAME, 0);
        setAccountName(mSettings.getString(PREF_ACCOUNT_NAME, null));

        // TODO: Pass mCredential as the last parameter instead of null.
        Graderecorder.Builder builder = new Graderecorder.Builder(
                AndroidHttp.newCompatibleTransport(), new GsonFactory(), mCredential);
        mService = builder.build();

        if (mCredential.getSelectedAccountName() == null) {
            Log.d(GR, "enter");

            // Not signed in, show login window or request an existing account.
            chooseAccount();
        }


        else{
            Log.d(GR, "mCredential:" + mCredential.getSelectedAccountName());
            Log.d(GR, "update Assigments");
            updateAssignments();
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(GR, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private void setToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setIcon(R.mipmap.ic_home);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((TextView) mToolbar.findViewById(R.id.toolbar_title)).setText(R.string.assigments_title);

    }

    private void updateAssignments() {
        mLoadingForm.setVisibility(View.VISIBLE);
        mEmpty.setVisibility(View.GONE);
        mListView.setVisibility(View.GONE);
        new QueryForAssignmentsTask().execute();
    }

    /**
     * Save the account name in preferences and the credentials
     *
     * @param accountName
     */
    private void setAccountName(String accountName) {
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(PREF_ACCOUNT_NAME, accountName);
        editor.commit();
        mCredential.setSelectedAccountName(accountName);
    }

    void chooseAccount() {
        // This picker is built in to the Android framework.
        startActivityForResult(mCredential.newChooseAccountIntent(), REQUEST_ACCOUNT_PICKER);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.assignment_list, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_ACCOUNT_PICKER:
                if (data != null && data.getExtras() != null) {
                    Log.d(GR, "data is not nul");
                    String accountName = data.getExtras().getString(AccountManager.KEY_ACCOUNT_NAME);
                    Log.d(GR, "account name:" + accountName);
                    if (accountName != null) {
                        Log.d(GR, "enter account name");
                        setAccountName(accountName); // User is authorized.
                        Log.d(GR, "update Assigments Result");
                        updateAssignments();
                    }
                }
                break;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_assignment_add:
                addAssignment();
                return true;
            case R.id.menu_assignment_sync:
                updateAssignments();
                return true;
            case R.id.menu_assignment_login:
                chooseAccount();
                return true;
            case R.id.menu_gcm:
                Intent mIntent = new Intent(AssignmentListActivity.this, GcmActivity.class);
                startActivity(mIntent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Dialog to add an assignment.
    private void addAssignment() {
        DialogFragment df = new DialogFragment() {
            @Override
            public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                     Bundle savedInstanceState) {
                View view = inflater.inflate(R.layout.dialog_add_assignment, container);
                getDialog().setTitle(R.string.dialog_add_assignment_title);
                final Button confirmButton = (Button) view
                        .findViewById(R.id.dialog_add_assignment_ok);
                final Button cancelButton = (Button) view
                        .findViewById(R.id.dialog_add_assignment_cancel);
                final EditText assignmentNameEditText = (EditText) view
                        .findViewById(R.id.dialog_add_assignment_name);

                confirmButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String name = assignmentNameEditText.getText().toString();
                        Toast.makeText(AssignmentListActivity.this,
                                "Got the assignment named " + name, Toast.LENGTH_LONG).show();
                        // add the data and send to server
                        Assignment assignment = new Assignment();
                        assignment.setName(name);
                        // The owner will be set in the backend.
                        ((AssignmentArrayAdapter) adapter).add(assignment);
                        ((AssignmentArrayAdapter) adapter).notifyDataSetChanged();
                        new InsertAssignmentTask().execute(assignment);
                        dismiss();
                    }
                });

                cancelButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismiss();
                    }
                });
                return view;
            }
        };
        df.show(getFragmentManager(), "");

    }

    // Our standard listener to delete multiple items.
    private class MyMultiClickListener implements MultiChoiceModeListener {

        private ArrayList<Assignment> mAssignmentsToDelete = new ArrayList<Assignment>();

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.context, menu);
            mode.setTitle(R.string.context_delete_title);
            return true; // gives tactile feedback
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.context_delete:
                    deleteSelectedItems();
                    mode.finish();
                    return true;
            }
            return false;
        }

        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int position, long id,
                                              boolean checked) {


            Assignment item = (Assignment) adapter.getItem(position);
            if (checked) {

                mAssignmentsToDelete.add(item);
            } else {
                mAssignmentsToDelete.remove(item);
            }
            mode.setTitle("Selected " + mAssignmentsToDelete.size() + " Assignments");
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            // purposefully empty
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            mAssignmentsToDelete = new ArrayList<Assignment>();
            return true;
        }

        private void deleteSelectedItems() {
            for (Assignment assignment : mAssignmentsToDelete) {
                ((ArrayAdapter<Assignment>) adapter).remove(assignment);
                new DeleteAssignmentTask().execute(assignment.getEntityKey());
            }
            ((ArrayAdapter<Assignment>) adapter).notifyDataSetChanged();
        }
    }

    // TODO: Make a way to edit assignments: short and long press actions
    // already used. Could add item to multiclick listener, but we probably
    // don't want to edit multiple items at once.

    // ---------------------------------------------------------------------------------
    // Backend communication
    // ---------------------------------------------------------------------------------

    class QueryForAssignmentsTask extends AsyncTask<Void, Void, AssignmentCollection> {

        @Override
        protected AssignmentCollection doInBackground(Void... unused) {
            AssignmentCollection assignments = null;
            try {
                // The logs are here to help debug authentication issues I
                // had...
                // Need qualification here for import below to work unqualified,
                // since there are two identically-named Assignment classes, in
                // the service and the model.
                Log.d(GR, "Using account name = " + mCredential.getSelectedAccountName());
                Graderecorder.Assignment.List query = mService.assignment().list();
                Log.d(GR, "Query = " + (query == null ? "null " : query.toString()));
                query.setLimit(50L);
                assignments = query.execute();
                Log.d(GR, "Assignments = " + assignments);

            } catch (IOException e) {
                Log.d(GR, "Failed loading " + e, e);

            }
            return assignments;
        }

        @Override
        protected void onPostExecute(AssignmentCollection result) {
            super.onPostExecute(result);
            if (result == null) {
                mEmpty.setText("Failed loading, result is null");
                Log.d(GR, "Failed loading, result is null");
                return;
            }

            if (result.getItems() == null) {
                Log.d(GR, "result get items is null");
                result.setItems(new ArrayList<Assignment>());
            }


            adapter = new AssignmentArrayAdapter(
                    AssignmentListActivity.this, android.R.layout.simple_list_item_activated_1,
                    result.getItems());
            mListView.setAdapter(adapter);


            mLoadingForm.setVisibility(View.GONE);

            if(adapter.isEmpty()) {
                mEmpty.setVisibility(View.VISIBLE);
            }
            else {
                mListView.setVisibility(View.VISIBLE);
            }

        }
    }

    class InsertAssignmentTask extends AsyncTask<Assignment, Void, Assignment> {

        @Override
        protected Assignment doInBackground(Assignment... items) {
            try {
                Assignment assignment = mService.assignment().insert(items[0]).execute();
                return assignment;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Assignment result) {
            super.onPostExecute(result);
            if (result == null) {
                Log.d(GR, "Error inserting assignment, result is null");
                return;
            }
            updateAssignments();
        }

    }

    class DeleteAssignmentTask extends AsyncTask<String, Void, Assignment> {

        @Override
        protected Assignment doInBackground(String... entityKeys) {
            Assignment returnedAssignment = null;

            try {
                returnedAssignment = mService.assignment().delete(entityKeys[0]).execute();
            } catch (IOException e) {
                Log.d(GR, "Failed deleting " + e);
            }
            return returnedAssignment;
        }

        @Override
        protected void onPostExecute(Assignment result) {
            super.onPostExecute(result);
            if (result == null) {
                Log.d(GR, "Failed deleting, result is null");
                return;
            }
            updateAssignments();
        }
    }
}
