package com.sddm.graderecorder.activities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.appspot.grade_recorder_sddm.graderecorder.Graderecorder;
import com.appspot.grade_recorder_sddm.graderecorder.model.Assignment;
import com.appspot.grade_recorder_sddm.graderecorder.model.GradeEntry;
import com.appspot.grade_recorder_sddm.graderecorder.model.GradeEntryCollection;
import com.appspot.grade_recorder_sddm.graderecorder.model.Student;
import com.appspot.grade_recorder_sddm.graderecorder.model.StudentCollection;
import com.sddm.graderecorder.R;
import com.sddm.graderecorder.adapters.AssignmentArrayAdapter;
import com.sddm.graderecorder.adapters.GradeEntryArrayAdapter;
import com.sddm.graderecorder.adapters.StudentAdapter;

import org.w3c.dom.Text;

public class GradeEntryListActivity extends AppCompatActivity {

    Toolbar mToolbar;
    ListView mListView;
    LinearLayout mLoadingForm;
    TextView mEmpty;

    GradeEntryArrayAdapter adapter;
    private String mAssignmentKey;

    private Map<String, Student> mStudentMap;
    private List<Student> mStudents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grade_entry);

        setToolbar();

        mListView = (ListView) findViewById(R.id.my_listview);
        mLoadingForm = (LinearLayout) findViewById(R.id.empty_form);
        mEmpty = (TextView) findViewById(R.id.no_assigments_label);

        mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        mListView.setMultiChoiceModeListener(new MyMultiClickListener());

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // TODO: Edit the grade entry via a dialog.
                GradeEntry gradeEntry = (GradeEntry) adapter.getItem(position);
                editGradeEntry(gradeEntry);
            }
        });

        // We'll need the assignment ID to query for assignments.
        Intent intent = getIntent();
        mAssignmentKey = intent.getStringExtra(AssignmentListActivity.KEY_ASSIGNMENT_ENTITY_KEY);
        String assignmentName = intent.getStringExtra(AssignmentListActivity.KEY_ASSIGNMENT_NAME);
        ((TextView) mToolbar.findViewById(R.id.toolbar_title)).setText(assignmentName);

        updateStudents();
        updateGradeEntries();
    }

    private void updateStudents() {
        new QueryForStudentsTask().execute();
    }

    private void updateGradeEntries() {
        mLoadingForm.setVisibility(View.VISIBLE);
        mEmpty.setVisibility(View.GONE);
        mListView.setVisibility(View.GONE);
        new QueryForGradeEntriesTask().execute(mAssignmentKey);
    }


    private void setToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setIcon(R.mipmap.ic_home);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) mToolbar.findViewById(R.id.toolbar_title)).setText(R.string.title_activity_grade_entry);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.grade_entry_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_grade_entry_add:
                addGradeEntry();
                return true;
            case R.id.menu_grade_entry_sync:
                updateGradeEntries();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void addGradeEntry() {
        DialogFragment df = new DialogFragment() {
            @Override
            public Dialog onCreateDialog(Bundle savedInstanceState) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(R.string.dialog_add_grade_entry_title);
                View view = getLayoutInflater().inflate(R.layout.dialog_add_grade_entry, null);
                final EditText scoreEditText = (EditText) view.findViewById(R.id.dialog_add_grade_entry_score);
                final Spinner nameSpinner = (Spinner) view.findViewById(R.id.dialog_add_grade_entry_student_spinner);

                // Map orders students by KEY (or comparators on keys), not username,
                // So I build and sort the list and use it.
                StudentAdapter studentAdapter = new StudentAdapter(GradeEntryListActivity.this, mStudents);
                studentAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                nameSpinner.setAdapter(studentAdapter);

                builder.setView(view);
                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String studentKey = ((Student) nameSpinner.getSelectedItem()).getEntityKey();
                        long score = Long.parseLong(scoreEditText.getText().toString());
                        // add the data and send to server
                        GradeEntry gradeEntry = new GradeEntry();
                        gradeEntry.setStudentKey(studentKey);
                        gradeEntry.setScore(score);
                        // Fails without the assignment ID!
                        gradeEntry.setAssignmentKey(mAssignmentKey);
                        ((GradeEntryArrayAdapter) adapter).add(gradeEntry);
                        ((GradeEntryArrayAdapter) adapter).notifyDataSetChanged();
                        new InsertGradeEntryTask().execute(gradeEntry);
                        dismiss();
                    }
                });
                builder.setNegativeButton(android.R.string.cancel, null);
                return builder.create();
            }
        };
        df.show(getFragmentManager(), "");
    }


    private void editGradeEntry(final GradeEntry gradeEntry) {
        DialogFragment df = new DialogFragment() {
            @Override
            public Dialog onCreateDialog(Bundle savedInstanceState) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(R.string.dialog_edit_grade_entry_title);
                View view = getLayoutInflater().inflate(R.layout.dialog_edit_grade_entry, null);
                final EditText scoreEditText = (EditText) view.findViewById(R.id.dialog_edit_grade_entry_score);
                final TextView nameTextView = (TextView) view.findViewById(R.id.dialog_edit_grade_entry_student_textView);

                Student mStudent =  mStudentMap.get(gradeEntry.getStudentKey());
                nameTextView.setText(mStudent.getFirstName() + " " + mStudent.getLastName());

                scoreEditText.setHint(String.valueOf(gradeEntry.getScore()));

                builder.setView(view);
                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        long score = Long.parseLong(scoreEditText.getText().toString());
                        // edit the data and send to server
                        gradeEntry.setScore(score);
                        ((GradeEntryArrayAdapter) adapter).notifyDataSetChanged();
                        new InsertGradeEntryTask().execute(gradeEntry);
                        dismiss();
                    }
                });
                builder.setNegativeButton(android.R.string.cancel, null);
                return builder.create();
            }
        };
        df.show(getFragmentManager(), "");
    }

    // Our standard listener to delete multiple items.
    private class MyMultiClickListener implements MultiChoiceModeListener {

        private ArrayList<GradeEntry> mGradeEntriesToDelete = new ArrayList<GradeEntry>();

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.context, menu);
            mode.setTitle(R.string.context_delete_title);
            return true; // gives tactile feedback
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.context_delete:
                    deleteSelectedItems();
                    mode.finish();
                    return true;
            }
            return false;
        }

        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
            GradeEntry item = (GradeEntry) adapter.getItem(position);
            if (checked) {
                mGradeEntriesToDelete.add(item);
            } else {
                mGradeEntriesToDelete.remove(item);
            }
            mode.setTitle("Selected " + mGradeEntriesToDelete.size() + " entries");
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            // purposefully empty
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            mGradeEntriesToDelete = new ArrayList<GradeEntry>();
            return true;
        }

        private void deleteSelectedItems() {
            for (GradeEntry gradeEntry : mGradeEntriesToDelete) {
                ((ArrayAdapter<GradeEntry>) adapter).remove(gradeEntry);
                new DeleteGradeEntryTask().execute(gradeEntry.getEntityKey());
            }
            ((ArrayAdapter<GradeEntry>) adapter).notifyDataSetChanged();
        }
    }

    // ---------------------------------------------------------------------------------
    // Backend communication
    // ---------------------------------------------------------------------------------

    class QueryForGradeEntriesTask extends AsyncTask<String, Void, GradeEntryCollection> {

        @Override
        protected GradeEntryCollection doInBackground(String... entityKeys) {
            GradeEntryCollection gradeEntries = null;
            try {
                Graderecorder.Gradeentry.List query = AssignmentListActivity.mService.gradeentry().list(
                        entityKeys[0]);
                query.setLimit(50L);
                gradeEntries = query.execute();
                Log.d(AssignmentListActivity.GR, "Grade entries = " + gradeEntries);

            } catch (IOException e) {
                Log.d(AssignmentListActivity.GR, "Failed loading " + e, e);

            }
            return gradeEntries;
        }

        @Override
        protected void onPostExecute(GradeEntryCollection result) {
            super.onPostExecute(result);
            if (result == null) {
                mEmpty.setText("Failed loading, result is null");
                Log.d(AssignmentListActivity.GR, "Failed loading, result is null");
                return;
            }

            if (result.getItems() == null) {
                Log.d(AssignmentListActivity.GR, "result get items is null");
                result.setItems(new ArrayList<GradeEntry>());
            }

            adapter = new GradeEntryArrayAdapter(GradeEntryListActivity.this,
                    android.R.layout.simple_list_item_1, result.getItems(), mStudentMap);
            mListView.setAdapter(adapter);

            mLoadingForm.setVisibility(View.GONE);

            if(adapter.isEmpty()) {
                mEmpty.setVisibility(View.VISIBLE);
            }
            else {
                mListView.setVisibility(View.VISIBLE);
            }
        }
    }

    class InsertGradeEntryTask extends AsyncTask<GradeEntry, Void, GradeEntry> {

        @Override
        protected GradeEntry doInBackground(GradeEntry... items) {
            try {
                GradeEntry gradeEntry = AssignmentListActivity.mService.gradeentry().insert(items[0]).execute();
                return gradeEntry;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(GradeEntry result) {
            super.onPostExecute(result);
            if (result == null) {
                Log.d(AssignmentListActivity.GR, "Error inserting grade entry, result is null");
                return;
            }
            updateGradeEntries();
        }
    }

    class DeleteGradeEntryTask extends AsyncTask<String, Void, GradeEntry> {

        @Override
        protected GradeEntry doInBackground(String... entityKeys) {
            GradeEntry returnedGradeEntry = null;

            try {
                returnedGradeEntry = AssignmentListActivity.mService.gradeentry().delete(entityKeys[0]).execute();
            } catch (IOException e) {
                Log.d(AssignmentListActivity.GR, "Failed deleting " + e, e);
            }
            return returnedGradeEntry;
        }

        @Override
        protected void onPostExecute(GradeEntry result) {
            super.onPostExecute(result);
            if (result == null) {
                Log.d(AssignmentListActivity.GR, "Failed deleting, result is null");
                return;
            }
            updateGradeEntries();
        }
    }

    class QueryForStudentsTask extends AsyncTask<Void, Void, StudentCollection> {

        @Override
        protected StudentCollection doInBackground(Void... unused) {
            StudentCollection students = null;
            try {
                Graderecorder.Student.List query = AssignmentListActivity.mService.student().list();
                Log.d(AssignmentListActivity.GR, "Query = " + (query == null ? "null " : query.toString()));
                query.setLimit(50L);
                students = query.execute();
                Log.d(AssignmentListActivity.GR, "Students = " + students);

            } catch (IOException e) {
                Log.d(AssignmentListActivity.GR, "Failed loading " + e, e);

            }
            return students;
        }

        @Override
        protected void onPostExecute(StudentCollection result) {
            super.onPostExecute(result);
            if (result == null) {
                mEmpty.setText("Failed loading, result is null");
                Log.d(AssignmentListActivity.GR, "Failed loading, result is null");
                return;
            }

            if (result.getItems() == null) {
                Log.d(AssignmentListActivity.GR, "result get items is null");
                result.setItems(new ArrayList<Student>());
            }


            // Store students for later use.
            mStudents = new ArrayList<Student>();
            mStudentMap = new TreeMap<String, Student>();
            for (Student s : result.getItems()) {
                mStudentMap.put(s.getEntityKey(), s);
                mStudents.add(s);
            }
            Comparator<Student> comp = new Comparator<Student>() {
                @Override
                public int compare(Student lhs, Student rhs) {
                    return lhs.getRoseUsername().compareTo(rhs.getRoseUsername());
                }
            };
            Collections.sort(mStudents, comp);
        }
    }


}
