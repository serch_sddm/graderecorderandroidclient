/*
package com.sddm.graderecorder.activities;

import android.accounts.AccountManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.appspot.grade_recorder_sddm.graderecorder.Graderecorder;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.sddm.graderecorder.QuickstartPreferences;
import com.sddm.graderecorder.R;
import com.sddm.graderecorder.RegistrationIntentService;
import com.sddm.graderecorder.request.RegisterRequest;
import com.sddm.graderecorder.rest.RegisterRequestDTO;
import com.sddm.graderecorder.rest.RegisterResponseDTO;
import com.sddm.graderecorder.utils.AlertDialogUtils;
import com.sddm.graderecorder.utils.UIHelper;

public class WelcomeActivity extends BaseActivity implements View.OnClickListener {

    String name;
    String email;
    String register_id;

    EditText mName;
    TextView mEmail;

    LinearLayout progressView;
    LinearLayout tokenProgressView;
    LinearLayout bodyView;

    Toolbar mToolbar;

    */
/**
     * Credentials object that maintains tokens to send to the back end.
     *//*

    GoogleAccountCredential mCredential;

    static final int REQUEST_ACCOUNT_PICKER = 1;

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ProgressBar mRegistrationProgressBar;
    private TextView mInformationTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        ((Button)findViewById(R.id.btn_access)).setOnClickListener(this);

        progressView = (LinearLayout) findViewById(R.id.access_progress_form);
        bodyView = (LinearLayout) findViewById(R.id.body_view);
        tokenProgressView = (LinearLayout)findViewById(R.id.token_progress_form);

        mInformationTextView = (TextView) findViewById(R.id.informationTextView);

        mName = (EditText)findViewById(R.id.et_name);
        mEmail = (TextView)findViewById(R.id.tv_email);

        if (checkPlayServices()) {
            Log.d(TAG, "you have play services" );

            mRegistrationBroadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    mRegistrationProgressBar.setVisibility(ProgressBar.GONE);
                    SharedPreferences sharedPreferences =
                            PreferenceManager.getDefaultSharedPreferences(context);
                    boolean sentToken = sharedPreferences
                            .getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
                    String token = sharedPreferences
                            .getString(QuickstartPreferences.TOKEN_TO_SERVER, null);
                    if (sentToken && !(token.isEmpty())) {
                       tokenProgressView.setVisibility(View.GONE);
                        register_id = token;
                        Toast.makeText(WelcomeActivity.this, getString(R.string.gcm_send_message), Toast.LENGTH_LONG).show();
                        chooseAccount();
                    } else {
                        mInformationTextView.setText(getString(R.string.token_error_message));
                    }
                }
            };

            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    private void setToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setIcon(R.mipmap.ic_home);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((TextView) mToolbar.findViewById(R.id.toolbar_title)).setText(R.string.app_name);

    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.btn_access){
            name = mName.getText().toString();
            email = mEmail.getText().toString();

            showProgress(true);

            attemptRegister();
        }
    }

    void chooseAccount() {
        // This picker is built in to the Android framework.
        startActivityForResult(mCredential.newChooseAccountIntent(), REQUEST_ACCOUNT_PICKER);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_ACCOUNT_PICKER:
                if (data != null && data.getExtras() != null) {
                    Log.d(TAG, "data is not nul");
                    String accountName = data.getExtras().getString(AccountManager.KEY_ACCOUNT_NAME);
                    Log.d(TAG, "account name:" + accountName);
                    if (accountName != null) {
                        Log.d(TAG, "enter account name");
                        setAccountName(accountName); // User is authorized.
                        mEmail.setText(accountName);
                    }
                }
                break;
        }
    }

    public static final String PREF_ACCOUNT_NAME = "PREF_ACCOUNT_NAME";
    */
/**
     * Save the account name in preferences and the credentials
     *
     * @param accountName
     *//*

    private void setAccountName(String accountName) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_ACCOUNT_NAME, accountName);
        editor.commit();
    }


    public void showProgress(final boolean show) {
        UIHelper.showProgress(show, bodyView, progressView, getResources());
    }


    private void attemptRegister() {

        RegisterRequest request = new RegisterRequest(new RegisterRequestDTO(email,name,register_id), getResources());
        getSpiceManager().execute(request, new RegisterRequestListener());
    }

    AlertDialog.Builder alertView;

    private final class RegisterRequestListener implements RequestListener<RegisterResponseDTO> {

        @Override
        public void onRequestFailure(SpiceException spiceException) { //login response is unsuccessful

            showProgress(false); //hides the waiting progress dialog

            //displays a feedback message on the screen
            alertView = AlertDialogUtils.initAlertDialog(WelcomeActivity.this, getString(R.string.feedback_warning), "Error: " + spiceException.getMessage(), R.drawable.ic_dialog_alert_holo_light);
            alertView.setPositiveButton(getString(R.string.feedback_accept), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertView.show();
        }

        @Override
        public void onRequestSuccess(RegisterResponseDTO result) { //login response is successful

            if (!result.getEntityKey().isEmpty()) { //gets a correct code

              */
/*  AuthenticationManager.authenticate(LoginActivity.this, facebookToken, result.getToken(), username); //stores the authentication tokens for this session.
                AuthenticationManager.persistUserInfo(LoginActivity.this, mInfo.getName(), mInfo.getLast_name(), mInfo.getCountry(), email);*//*


                Intent myIntent = new Intent(WelcomeActivity.this, Graderecorder.Assignment.class);
                myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(myIntent);
                showProgress(false); // hide the waiting progress dialog
                finish();
            } else {  //gets an incorrect code

                showProgress(false); // hide the loading progress dialog

                //displays a feedback message on the screen
                alertView = AlertDialogUtils.initAlertDialog(WelcomeActivity.this, getString(R.string.feedback_warning), getString(R.string.feedback_error), R.drawable.ic_dialog_alert_holo_light);
                alertView.setPositiveButton(getString(R.string.feedback_accept), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertView.show();
            }
        }
    }


    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "LogActivity";
    */
/**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     *//*

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
}
*/
