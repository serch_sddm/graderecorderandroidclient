package com.sddm.graderecorder.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sddm.graderecorder.helpers.QuickstartPreferences;
import com.sddm.graderecorder.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;


public class GcmActivity extends AppCompatActivity implements View.OnClickListener {

    Toolbar mToolbar;

    String token;
    String externalToken;
    EditText mMessage;

    private final String MY_TAG = GcmActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gcm);

        setToolbar();

        SharedPreferences sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        token = sharedPreferences
                .getString(QuickstartPreferences.TOKEN_TO_SERVER, null);

        ((Button) findViewById(R.id.gcm_this)).setOnClickListener(this);
        ((Button) findViewById(R.id.gcm_unicast)).setOnClickListener(this);
        ((Button) findViewById(R.id.gcm_broadcast)).setOnClickListener(this);
        ((TextView) findViewById(R.id.tv_token)).setText(token);
        mMessage = (EditText) findViewById(R.id.et_message);
    }


    private void setToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setIcon(R.mipmap.ic_home);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) mToolbar.findViewById(R.id.toolbar_title)).setText(R.string.gcm_title);

    }

    @Override
    public void onClick(View v) {

        String message = mMessage.getText().toString();

        int id = v.getId();
        if (id == R.id.gcm_this) {
            SenderAsyncTask asyncT = new SenderAsyncTask();
            asyncT.execute(message, token);

        } else if (id == R.id.gcm_unicast) {
            getExternalToken(message);

        } else if (id == R.id.gcm_broadcast) {
            SenderAsyncTask asyncT = new SenderAsyncTask();
            asyncT.execute(message);
        }
    }

    private void getExternalToken(final String msg) {

        DialogFragment df = new DialogFragment() {
            @Override
            public Dialog onCreateDialog(Bundle savedInstanceState) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(R.string.dialog_get_token_title);
                View view = getLayoutInflater().inflate(R.layout.dialog_get_token, null);
                final EditText tokenEditText = (EditText) view.findViewById(R.id.dialog_get_token_et);

                builder.setView(view);
                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        externalToken = tokenEditText.getText().toString();
                        SenderAsyncTask asyncT = new SenderAsyncTask();
                        asyncT.execute(msg, externalToken);
                        dismiss();
                    }
                });
                builder.setNegativeButton(android.R.string.cancel, null);
                return builder.create();
            }
        };
        df.show(getFragmentManager(), "");
    }

    String responseServer;

    /* Inner class to get response */
    class SenderAsyncTask extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... args) {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("https://android.googleapis.com/gcm/send");

            try {
                JSONObject jsonobj = new JSONObject();
                JSONObject childData = new JSONObject();

                childData.put("message", args[0].trim());

                if (args.length > 1) {
                    jsonobj.put("to", args[1].trim());
                } else {
                    jsonobj.put("to", "/topics/global");
                }

                jsonobj.put("data", childData);

                StringEntity se = new StringEntity(jsonobj.toString());
                Log.d(MY_TAG, "mainToPost:" + se.toString());

                // Use UrlEncodedFormEntity to send in proper format which we need
                httppost.setHeader("Authorization", "key=AIzaSyBv-JPkkMYaZU5ohUH-Vo7e2ZIGATrmYeM");
                httppost.setHeader("Content-Type", "application/json");
                httppost.setEntity(se);

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                responseServer = str.getStringFromInputStream(inputStream);
                Log.d(MY_TAG, "response -----" + responseServer);


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            Log.d(MY_TAG, "response:" + responseServer);
        }
    }

    public static class InputStreamToStringExample {

        public static void main(String[] args) throws IOException {

            // intilize an InputStream
            InputStream is =
                    new ByteArrayInputStream("file content..blah blah".getBytes());

            String result = getStringFromInputStream(is);

            System.out.println(result);
            System.out.println("Done");

        }

        // convert InputStream to String
        private static String getStringFromInputStream(InputStream is) {

            BufferedReader br = null;
            StringBuilder sb = new StringBuilder();

            String line;
            try {

                br = new BufferedReader(new InputStreamReader(is));
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return sb.toString();
        }

    }
}
