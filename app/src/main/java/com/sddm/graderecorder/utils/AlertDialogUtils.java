package com.sddm.graderecorder.utils;

import android.support.v7.app.AlertDialog;
import android.content.Context;
import android.util.Log;


public class AlertDialogUtils{

    static AlertDialog.Builder feedBackDialog;

    public  static AlertDialog.Builder initAlertDialog(Context context, String title, String message, int icon) {


        feedBackDialog = new AlertDialog.Builder(context);

        feedBackDialog.setTitle(title);
        feedBackDialog.setMessage(message);
        feedBackDialog.setCancelable(false);
        feedBackDialog.setIcon(context.getResources().getDrawable(icon));

        return feedBackDialog;

    }

    public void show(){
        feedBackDialog.show();
    }
}
