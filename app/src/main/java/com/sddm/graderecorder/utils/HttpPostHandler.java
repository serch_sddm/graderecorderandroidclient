package com.sddm.graderecorder.utils;

import android.util.Log;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by sduran on 01/12/2015.
 */
public class HttpPostHandler {


    public static void makePostRequest() throws IOException, JSONException {

        URL url = new URL("https://gcm-http.googleapis.com/gcm/send");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestProperty("Authorization", "key=AIzaSyBv-JPkkMYaZU5ohUH-Vo7e2ZIGATrmYeM");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);


        JSONObject jGcmData = new JSONObject();
        JSONObject jData = new JSONObject();

        jGcmData.put("to", "/topics/global");
        jData.put("message", "testing");
        jGcmData.put("data", jData);

        OutputStream outputStream = conn.getOutputStream();
        outputStream.write(jGcmData.toString().getBytes());

        InputStream inputStream = conn.getInputStream();
        String resp = IOUtils.toString(inputStream);
        Log.d("Ejele", "resp" + resp);
        return;
    }
}
