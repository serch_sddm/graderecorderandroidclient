package com.sddm.graderecorder.adapters;

import java.util.List;


import android.content.Context;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.appspot.grade_recorder_sddm.graderecorder.model.Assignment;
import com.sddm.graderecorder.R;
import com.sddm.graderecorder.views.AssignmentEntryView;

public class AssignmentArrayAdapter extends ArrayAdapter<Assignment> {

	private Context mContext;

	public AssignmentArrayAdapter(Context context, int resource, List<Assignment> assignments) {
		super(context, resource, assignments);
		mContext = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		AssignmentEntryView view = null;
		if (convertView == null) {
			view = new AssignmentEntryView(mContext);
		} else {
			view = (AssignmentEntryView) convertView;
		}
		Assignment assignment = getItem(position);

		view.setAssignmentName(assignment.getName());
		return view;
	}

}
