package com.sddm.graderecorder.views;

import android.app.Activity;
import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sddm.graderecorder.R;

public class AssignmentEntryView extends RelativeLayout {

    private TextView assignmentTextView;

    public AssignmentEntryView(Context context) {
        super(context);
        ((Activity) context).getLayoutInflater().inflate(R.layout.assignment_entry_view, this);
        assignmentTextView = (TextView) findViewById(R.id.assignment_entry_name);
    }

    public void setAssignmentName(String name) {
        assignmentTextView.setText(name);
    }


}
