/*
package com.sddm.graderecorder.request;

import android.content.res.Resources;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;
import com.sddm.graderecorder.R;
import com.sddm.graderecorder.rest.RegisterRequestDTO;
import com.sddm.graderecorder.rest.RegisterResponseDTO;

public class RegisterRequest extends SpringAndroidSpiceRequest<RegisterResponseDTO> {

    private RegisterRequestDTO registerRequest;
    private Resources resources;

    public RegisterRequest(RegisterRequestDTO request, Resources resources) {
        super(RegisterResponseDTO.class);
        this.registerRequest = request;
        this.resources = resources;
    }

    @Override
    public RegisterResponseDTO loadDataFromNetwork() throws Exception {
        String url = resources.getString(R.string.endpoint) + resources.getString(R.string.resource_professor_insert);
        RegisterResponseDTO response = getRestTemplate().postForObject(url, registerRequest, RegisterResponseDTO.class);
        return response;
    }
}
*/
